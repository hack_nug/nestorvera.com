---
layout: post
title:  'Adhara'
year:   '2014'
domain: 'http://adhara.es'
categories: website wordpress
status: published
---

Adhara és un centre d'estètica del barri de Gràcia i volien una web on poder presentar els serveis que ofereixen als seus clients i les marques amb les que treballen, entre altres coses.
