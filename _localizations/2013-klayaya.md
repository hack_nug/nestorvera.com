---
layout: post
title:  'KLaYaya'
year:   '2013'
domain: 'http://klayaya.com'
categories: website wordpress
status: published
---

KLaYaya era (💀) un colectiu musical que servia de plataforma als diferents artistes que grabaven al mateix estudi. La idea era tenir una pàgina on poder publicar tots els treballs realitzats allà i tot el contingut relacionat amb aquests.
