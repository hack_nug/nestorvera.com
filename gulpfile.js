var gulp        = require('gulp');
var shell       = require('gulp-shell');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;

var rename      = require('gulp-rename');
var concat      = require('gulp-concat');

var uglify      = require('gulp-uglify');
var insert      = require('gulp-insert');

var sass        = require('gulp-sass');
var cssmin      = require('gulp-clean-css');
var csslint     = require('gulp-csslint');
var prefix      = require('gulp-autoprefixer');

// Builds blog when something changes:
gulp.task('build', shell.task(['bundle exec jekyll build --config _config.yml,_config_dev.yml --watch']));
// Or if you don't use bundle:
// gulp.task('build', shell.task(['jekyll build --watch']));

// Serves blog using Browsersync:
gulp.task('serve', function () {
    browserSync.init({server: {baseDir: '_site/'}});
    // Reloads page when some of the already built files changed:
    gulp.watch('_site/**/*.*').on('change', reload);
});

// Checks for dependancies updates:
gulp.task('check',
  shell.task(['ncu --loglevel verbose --packageFile package.json']));

// Updates dependancies:
gulp.task('update',
    shell.task(['ncu -u --loglevel verbose --packageFile package.json']));
    // After updating it should place new files in /assets

// Concatenates and minifies js files
gulp.task('scripts', function () {
    return gulp.src(['assets/javascripts/*.js'])
        .pipe(concat('index.js'))
        .pipe(gulp.dest('\_includes/'))
        .pipe(rename({ extname: '.min.js' }))
        .pipe(uglify())
        .pipe(gulp.dest('\_includes/'));
});

// Concatenates and minifies css files
gulp.task('styles', function () {
    return gulp.src(['assets/stylesheets/main.scss'])
        .pipe(sass())
        .pipe(prefix("last 1 version", "> 1%", "ie 8"))
        .pipe(csslint())
        .pipe(cssmin())
        .pipe(rename('main.css'))
        .pipe(gulp.dest('\_includes/'));
});

/*
gulp.task('css', function () {
  var
    processors = [  ],
    nanoArgs = { autoprefixer: false },
    nextArgs = { autoprefixer: false }
  return gulp.src('./src/styles/css/*.css')
    // .pipe(postcss(processors))
    // .pipe(cssnano({nanoArgs}))
    .pipe(gulp.dest('./build/styles/css'))
    .pipe(connect.reload())
});
*/

// Watched assets folder for changes
gulp.task('watch', function () {
  gulp.watch('assets/javascripts/*.js', gulp.task('scripts'));
  gulp.watch('assets/stylesheets/*.scss', gulp.task('styles'));
});

// Default task
gulp.task('default', gulp.series('check', 'scripts', 'styles', gulp.parallel('build', 'serve', 'watch')));
