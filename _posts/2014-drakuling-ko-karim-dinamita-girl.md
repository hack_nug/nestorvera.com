---
layout: cover
status: published

title: Drakuling Ko-Karim — Dinamita Girl
description:

year: 2014
client: KLaYaya
collaborators:
  - KUOR Larraz
  - Sax

tools:
  - After Effects
  - Photoshop

category: art direction, cover, gif
tags: after effects, photoshop

link: http://www.klayaya.com/drakuling-ko-karim-dinamita-girl/
image: http://www.klayaya.com/wp-content/uploads/Portada-600.jpg
soundcloud:
youtube: https://youtu.be/Wvk0zB9RL9Q

tracklist:
  - "Dinamita Girl"

---

<iframe src="http://www.youtube.com/embed/Wvk0zB9RL9Q?rel=0" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
