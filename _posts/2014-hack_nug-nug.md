---
layout: cover
status: published

title: hack_nug — NUG
description:

year: 2014
client: KLaYaya
collaborators:
  - Agon_Beats
  - KUOR Larraz
  - Nele Tullus
  - Sax

tools:
  - After Effects

category: art direction, cover, gif
tags: after effects

link: http://www.klayaya.com/hack_nug-nug-chopped-by-kuor-larraz-prod-agon_beats/
image: http://zippy.gfycat.com/MindlessYellowishHippopotamus.gif
soundcloud: https://soundcloud.com/hack_nug/nug
youtube: https://youtu.be/O39aBJtiT6g

tracklist:
  - "NUG (Chopped by KUOR Larraz)"

---

<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/142167953%3Fsecret_token%3Ds-HZGqx&amp;color=111111&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false"></iframe>

<iframe src="http://www.youtube.com/embed/O39aBJtiT6g?rel=0" frameborder="0" allowfullscreen></iframe>
