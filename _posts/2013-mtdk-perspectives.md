---
layout: cover
status: published

title: MTDK — Perspectives
description:

year: 2013
client: KLaYaya
collaborators:
  - MTDK
  - Sax

tools:
  - Nikon 3100
  - Photoshop

category: art direction, cover, photgraphy
tags: photography, photoshop

link: http://www.klayaya.com/mtdk-perspectives/
image: http://www.klayaya.com/wp-content/uploads/MTDK-Groupees.jpg
soundcloud:
youtube:

tracklist:
  - "Black Rain"
  - "Inside A Memory"
  - "Hack — Pecador (MTDK Remix)"

---

<iframe width="100%" height="350" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F6900017&amp;color=665f5f&amp;auto_play=false&amp;show_artwork=false"></iframe>
