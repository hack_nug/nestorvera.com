---
layout: cover
status: published

title: KUOR — Remixes
description:

year: 2012
client: KLaYaya
collaborators:
  - KUOR Larraz
  - Sax
tools:
  - Photoshop

category: cover
tags: photoshop, blur

link: http://www.klayaya.com/kuor-remixes/
image: http://www.klayaya.com/wp-content/uploads/KUOR-%E2%80%94-Remixes.jpg
soundcloud: https://soundcloud.com/ku-beats/chirie-vegas-stardust-kuor
youtube:

tracklist:
  - "Chirie Vegas — Stardust (Over “Baby Be Mine”)"
  - "Eazy-E — Hittin’ Switches"
  - "Cecilio.G — Legal Drugz (Episode 2)"
  - "B.G. Knocc Out — Dat’s How I’m Livin’"
  - "Three 6 Mafia &amp; UGK — Sippin’ On Some Syrup"
  - "Eazy-E — Tha Muthaphukkin' Real"
  - "BONUS TRACK: Remixes Session (Feat. DJ Sax)"

---
