---
layout: cover
status: published

title: Yung Beef & Steve Lean — A.D.R.O.M.I.C.F.M.S.
description:

year: 2014
client: PXXR GVNG
collaborators:
  - Steve Lean
  - Yung Beef

tools:
  - InDesign
  - Photoshop

category: cover,
tags: indesign, photoshop

link:
image:
soundcloud:
youtube: https://youtu.be/HVnXpYyrBbQ

tracklist:
  - "Intro"
  - "Sad Molly"
  - "^.^"
  - "Rapping2Ya"
  - "Ratchet Luv (Amor De Yoli)"
  - "Cute"
  - "Skit Cute Hitler"
  - "El Secreto De La Vida (feat. Mon8bit)"
  - "TrapJaus (feat. Khaled)"
  - "Outro"
  - "Bonus Track - Cryin' Fo' Poor Love"

---

<iframe src="http://www.youtube.com/embed/videoseries?list=PLEmJMlRjXBn0FDMUGixPVBhxZTt0fuIHX" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
