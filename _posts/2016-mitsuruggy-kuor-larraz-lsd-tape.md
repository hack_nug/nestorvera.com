---
layout: cover
status: published

title: Mitsuruggy & KUOR Larraz — LSD Tape
description:

year: 2016
client: Mituruggy
collaborators:
  - KUOR Larraz
  - Mitsuruggy

tools:
  - Chroma
  - CodePen
  - CSSX
  - jQuery
  - Parallax.js
  - SASS

category: cover, website
tags: 

link: http://www.mitsuruggy.es/album/lsd-tape-ley-del-salto-dimensional/
image: http://www.mitsuruggy.es/wp-content/uploads/2016/04/Portada-Espaciolina.jpg
soundcloud:
youtube:
codepen: https://codepen.io/hack_nug/pen/zvNyQz

tracklist:
  - "Espaciolina"
  - "Warp Mind"
  - "Invisible"

---
