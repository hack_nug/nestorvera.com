---
layout: cover
status: published

title: Evan Malakay — Dirty Money
description:

year: 2013
client: KLaYaya
collaborators:
  - Bich
  - Evan Malakay
  - KUOR Larraz
  - Sax

tools:
  - Photoshop

category: cover
tags: photoshop, blur

link: http://www.klayaya.com/evan-malakay-dirty-money/
image: http://www.klayaya.com/wp-content/uploads/Evan-Malakay-Dirty-Money-Portada-Web.jpg
soundcloud: https://soundcloud.com/hardhustler/evan-malakay-dirty-money-full
youtube:

tracklist:
  - "Smoke Waves Intro"
  - "Boss Top"
  - "Blue Eyes"
  - "Dirty Money"
  - "Like Me"
  - "Quagmire (feat. Cecilio.G)"
  - "Ladies"
  - "Cheles & ALLSTRS"

---

<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F111314585&amp;color=FF0000&amp;auto_play=false&amp;show_artwork=false"></iframe>
