---
layout: cover
status: published

title: Extraño Weys — Variacions En Fu-Remoll De La 5ª Simfonia Inèdita De Roger Linn (Els Rèmics)
description:

year: 2015
client: Extraño Weys
collaborators:
  - Amazing Bèstia aka iLogike
  - Extraño Weys

tools:
  - Photoshop

category: cover,
tags: photoshop

link: https://hardlybrezrecords.bandcamp.com/album/amazing-b-stia-ew-revisions
image:
soundcloud:
youtube:
bandcamp: https://hardlybrezrecords.bandcamp.com/album/amazing-b-stia-ew-revisions

tracklist:
  - "El baul de l'iLogike"
  - "Sensei sequoia"
  - "Spaguetti automation"
  - "Rodalies fingers"
  - "L'ombra del quisquillós"
  - "Orivillian"

---
