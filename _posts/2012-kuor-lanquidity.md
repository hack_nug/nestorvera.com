---
layout: cover
status: published

title: KUOR x Sun Ra — Lanquidity
description:

year: 2012
client: KLaYaya
collaborators:
  - KUOR Larraz
  - Sax
tools:
  - Photoshop

category: cover
tags: photoshop, rework

link: http://www.klayaya.com/kuor-lanquidity/
image: http://www.klayaya.com/wp-content/uploads/KUOR-x-Sun-Ra-—-Lanquidity.jpg
soundcloud: https://soundcloud.com/ku-beats/lanquidity-feat-c-l-smooth
youtube:

tracklist:
  - "C.L. Smooth — Lanquidity"
  - "Free Murda — Where Pathways Meet"
  - "C.L. Smooth & Pete Rock — Twin Stars Of Thence"
  - "Raekwon & Fat Joe — That's How I Feel"

---

<iframe width="100%" height="350" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F3134222&amp;color=cc3333&amp;auto_play=false&amp;show_artwork=false"></iframe>
