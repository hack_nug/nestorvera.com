---
layout: cover
status: published

title: Creepy Boy — The Mixtape
description:

year: 2014
client: KLaYaya
collaborators:
  - Creepy Boy
  - Sax

tools:
  - After Effects
  - Illustrator

category: art direction, cover, gif
tags: after effects, illustrator

link: http://www.klayaya.com/creepy-boy-the-mixtape/
image: http://zippy.gfycat.com/SelfassuredHappygoluckyKentrosaurus.gif
soundcloud:
youtube: https://youtu.be/EnBBsk94ai4?list=PLEmJMlRjXBn0FDMUGixPVBhxZTt0fuIHX

tracklist:
  - "Freezing Moon"
  - "Evil Druid"
  - "Raindrop Feeling"
  - "En El Duelo"

---

<iframe src="http://www.youtube.com/embed/videoseries?list=PLEmJMlRjXBn0FDMUGixPVBhxZTt0fuIHX" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
