---
layout: cover
status: published

title: Agon_Beats & Vicente El Vizio — Remixes
description:

year: 2014
client: KLaYaya
collaborators:
  - Agon_Beats
  - Vicente El Vizio

tools:
  - Photoshop

category: art direction, cover, gif
tags: photoshop

link: http://www.klayaya.com/agon_beats-vicente-el-vizio-prendo-freezing-my-mind/
image:
  - http://www.klayaya.com/wp-content/uploads/Portada-Prendo-Web.jpg
  - http://www.klayaya.com/wp-content/uploads/Portada-Freezing-Web.jpg
soundcloud:
youtube: https://youtu.be/AbDrmook42k?list=PLEmJMlRjXBn37FjawjM_Zup6d-0x8jdxB

tracklist:
  - "Prendo"
  - "Freezing My Mind"

---

<iframe src="http://www.youtube.com/embed/videoseries?list=PLEmJMlRjXBn37FjawjM_Zup6d-0x8jdxB" frameborder="0" allowfullscreen></iframe>
