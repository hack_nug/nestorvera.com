---
layout: cover
status: published

title: The Black Iwn — The Last Evening
description:

year: 2014
client: KLaYaya
collaborators:
  - The Black Iwn
  - Sax

tools:
  - Photoshop

category: art direction, cover
tags: photoshop

link: http://www.klayaya.com/the-black-iwn-the-last-evening/
image: http://www.klayaya.com/wp-content/uploads/The-Black-IWN-600-Optimizado.jpg
soundcloud: https://soundcloud.com/theblackiwn/sets/the-black-iwn-the-last-evening
youtube: https://youtu.be/8FlmCFDppZk?list=PLEmJMlRjXBn2Byzk0RRhGDxckKku8XJd9

tracklist:
  - "4004"
  - "The Only Way To Become Great"
  - "Whitecaps"
  - "Death In The Eyes"
  - "When The Fall Is All That's Left…"
  - "The Last Evening"
  - "The Last Evening (Album Mix)"

---

<iframe width="100%" height="400" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/56223783%3Fsecret_token%3Ds-tswFc&color=111111&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=false&amp;show_artwork=false"></iframe>

<iframe src="http://www.youtube.com/embed/videoseries?list=PLEmJMlRjXBn2Byzk0RRhGDxckKku8XJd9" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
