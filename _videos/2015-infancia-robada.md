---
layout: video
title: Infancia Robada
description: Margarito wanted to do a photo shoot with Miguel from Los Miniboys and decided KUOR and I should be the ones taking care of the sound and visuals. Everything else you can see on video 👹
year: 2015
client: Margarito dela Guetto
collaborators:
  - Margarito dela Guetto
  - KUOR Larraz
  - Miguel (Los Miniboys)
tools:
  - iPhone 6
  - VHS Cam
  - Adobe Premiere
  - Adobe After Effects
  - YouTube
status: published
---

{% include youtube.html id="KldAqVkBIiU" ratio="75" %}
