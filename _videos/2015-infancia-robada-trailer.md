---
layout: video
title:  'Infancia Robada (Trailer)'
description: Vastned is a listed European retail property company with a focus on ‘Venues for Premium Shopping’. Vastned is listed on Euronext Amsterdam (AMX) with a property portfolio of approximately €1.7 billion.
year: 2015
client: Margarito dela Guetto
collaborators:
  - Margarito dela Guetto
  - KUOR Larraz
tools:
  - iPhone 6
  - VHS Cam
  - YouTube
status: published
---

{% include youtube.html id="cg0ys35wZKo" ratio="75" %}
