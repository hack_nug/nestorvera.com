---
layout: cover
title: 'KUOR — Remixes'
year: '2012'
domain: 'http://www.klayaya.com/wp-content/uploads/KUOR-%E2%80%94-Remixes.jpg'
categories: website wordpress
status: published
---

<ol>
  <li>Chirie Vegas — Stardust (Over “Baby Be Mine”)</li>
  <li>Eazy-E — Hittin’ Switches</li>
  <li>Cecilio.G — Legal Drugz (Episode 2)</li>
  <li>B.G. Knocc Out — Dat’s How I’m Livin’</li>
  <li>Three 6 Mafia &amp; UGK — Sippin’ On Some Syrup</li>
  <li>Eazy-E — Tha Muthaphukkin' Real</li>
  <li>BONUS TRACK: Remixes Session (Feat. DJ Sax)</li>
</ol>
