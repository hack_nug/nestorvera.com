---
layout: cover
title: 'The Black Iwn — The Last Evening'
year: '2014'
domain: 'http://www.klayaya.com/wp-content/uploads/The-Black-IWN-600-Optimizado.jpg'
categories: website wordpress
status: published
---

<div class="cf">
	<img class="fl w-100 w-two-thirds-l" src="{{ page.domain }}" alt="">

	<ol class="lh-copy fl w-100 w-third-l">
		<li>4004</li>
		<li>The Only Way To Become Great</li>
		<li>Whitecaps</li>
		<li>Death In The Eyes</li>
		<li>When The Fall Is All That's Left…</li>
		<li>The Last Evening</li>
		<li>The Last Evening (Album Mix)</li>
	</ol>

	<div class="fl w-100 w-two-thirds-l">
		<iframe width="100%" height="400" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/56223783%3Fsecret_token%3Ds-tswFc&color=111111&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=false&amp;show_artwork=false"></iframe>

		<iframe src="http://www.youtube.com/embed/videoseries?list=PLEmJMlRjXBn2Byzk0RRhGDxckKku8XJd9" width="300" height="150" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
	</div>
</div>
