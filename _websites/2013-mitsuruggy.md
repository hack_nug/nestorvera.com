---
layout: post
title:  'Mitsuruggy'
year:   '2013'
domain: 'http://mitsuruggy.com'
categories: website wordpress
status: published
---

Mitsuruggy és un artista madrileny amb més de dotze treballs publicats. Per a la seva pàgina volia una forma senzilla de publicar actualitzacions periòdiques, els seus treballs de descàrrega gratuita i enllaços a les tendes digitals on poden comprar-se la resta.
